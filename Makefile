# references:
#    BUILD_DIR and per-.cpp file https://spin.atomicobject.com/2016/08/26/makefile-c-projects/
awkbin     :=$(shell which awk)
cpbin      :=$(shell which cp)
echobin    :=$(shell which echo)
findbin    :=$(shell which find)
grepbin    :=$(shell which grep)
installbin :=$(shell which install)
rmbin      :=$(shell which rm)
sedbin     :=$(shell which sed)
sortbin    :=$(shell which sort)
truebin    :=$(shell which true)

CXX = g++

# to get full debug symbols, add -g to both FLAGS
CXXFLAGS = -g -std=c++11 -Wall -Weffc++ -Wextra -Wsign-conversion -Werror

# adding -s will make the linker strip the file
LDFLAGS = -g

src = $(wildcard *.cpp)
obj = $(src:.cpp=.o)

BUILD_DIR ?= .

OUTEXE = mine

all: $(OUTEXE)

# compile, which is not actually used?
$(BUILD_DIR)%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $+

# link
$(OUTEXE): $(obj)
	$(CXX) -o $@ $^ $(LDFLAGS)

.PHONY: clean cleanall list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$' -e '\.(cp*|o)'

clean:
	rm -f $(obj)

cleanall:
	rm -f $(obj) $(OUTEXE)
