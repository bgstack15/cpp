#include <iostream>
#include "math.h"

int getNumberFromUser()
{
   std::cout << "Enter an integer: " ;
   int x ;
   std::cin >> x ;
   return x ;
}

int doubleNumber(int x)
{
   //int z { 3 } ;
   //z = z * 2 ;
   return x * 2 ;
}
